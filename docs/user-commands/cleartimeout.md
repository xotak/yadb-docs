# Cleartimeout

## Description

Removes the timeout of an user

## Options

- user : The user that have been timed out

## Required permissions

- Moderate members