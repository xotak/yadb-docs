# Ban

## Description

Bans the selected member

## Options

- user : The user to ban (Required)
- reason : The reason of the ban

## Required permissions

- Ban members