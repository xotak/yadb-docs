# Kick

## Description

Kicks the selected member

## Options

- user : The user to kick (Required)
- reason : The reason of the kick

## Required permissions

- Kick members