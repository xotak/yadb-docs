# Timeout

## Description

Times the selected member out

## Options

- user : The user to timeout (Required)
- duration : The duration of the timeout in minutes (10 minutes default)
- reason : The reason of the timeout

## Required permissions

- Moderate members