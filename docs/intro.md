---
sidebar_position: 2
---

# Introduction

To add YADB to your Discord server, click on the following link. Be sure to check all the checkboxes to have all features available.

[Add to Discord](https://discord.com/api/oauth2/authorize?client_id=1174614219560341586&permissions=1393720151110&scope=bot+applications.commands)
