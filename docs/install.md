---
sidebar_position: 3
---

# Installation for self-hosting

Let's setup your own bot based on YADB! You do not need to do these steps if you use the hosted bot (YADB)

### What you'll need

- [Node.js](https://nodejs.org/en/download/) version 18.0 or above:
  - When installing Node.js, you are recommended to check all checkboxes related to dependencies.

## Creating a new bot application

First, you'll need to create an application on [Discord developper portal](https://discord.com/developers). Sign in to your account, and click the "New Application" button on top right corner of the website, next to your profile picture. Give it a name (It's not final and not the bot username.) and accept the Discord developper TOS. Then, a new page will appear with your apps detail. Head to the bot section. You can find the link on the website's sidebar. On this page, you can see all the infos of your bot. Here you can change the profile picture of the bot and the username. By default, the username of the bot is the same as the app's name. Scroll down a bit and find the section named "Privileged Gateway Intents". Toggle the "Server Member Intent" (the 2nd one). It is required user related features of the bot. Next step is resetting the token. Find the "Reset token" button under the username field and click it. Input your 2FA code if prompted. You will see a string of random characters. Copy it and keep it safe. It's the bot's password. You will be able to see it only once on that website.

:::danger Token sharing

The token is your bot's password, used to connect to Discord's server. When asking help online, never share it with anyone as people can run code on your bot's behalf.

:::

## Getting the bot's code

Then, to run the bot, you will need to get the code. You can easily download the latest version by following [this link](https://framagit.org/xotak/yadb/-/archive/main/yadb-main.zip). Once the download is finished, extract all the content of the zip in a folder.

If you know how to use Git, you can clone [this repository](https://framagit.org/xotak/yadb/).

## Installing Node.js dependencies

Open a shell. This can be cmd, PowerShell, bash or any other. Navigate to the folder you stored the bot's code in. Then, run `npm i`. This will install all the required code to run the bot.

## Creating configuration

Once the previous step is done, Copy the file called `default-config.json` located inside the `util` folder in the same folder as the `main.js` file. Open it in a text editor (Windows notepad is suitable for this)

```json title="Default config"
{
	"token": "your-token-goes-here",
  "clientId": "your-application-id-goes-here",
	"guildId": "your-server-id-goes-here"
}
```

In the file, you are going to replaces some values : 
- Instead of "your-token-goes-here", put your token, in double quotes

For the clientId and guildId, you will have to enable developer mode in your Discord's settings. It's in the "Advanced" section of the settings. Then, right-click on your server's icon and click on "Copy Server ID". Put that value double-quoted instead of "your-server-id-goes-here". Then go back to your Application settings on Developer Portal and find your Application ID. Copy it and put it in the place of "your-application-id-goes-here".

Now your bot is ready to be run, but some more steps are needed to make it fully working. Don't stop the tutorial here.

## Adding your bot to your server

On the Discord developper portal, go to your app's settings, then OAuth2 and URL Generator. In scope, check `bot` and `applications.commands`. In bot permission, Check the "Administrator" box. Finally, follow the link they give you at the bottom of the page, select your server and continue with the steps.

## Creating the database and deploying the bot

These are the final steps to run the bot : In a shell opened in the bot's directory, run the command `node dbInit.js` to create the database to store levels and `npm deploy` to send the list of commands to Discord.

## Launching the bot

In a shell, run `npm start`. If everything went well, the bot should log into Discord and start. You should see something like this : 

```
> yadb@0.1.0 start
> node main.js

[12:50:30.840] INFO (6790): Launched shard 0
[12:50:34.165] INFO (6820): Shard 0 ready! Logged in as <Bot username>
```

This will indicate that the bot have successfully started !

:::tip Uptime

The bot will be on only when the shell is open. If you close it or turn your computer off, the bot will stop working !

:::