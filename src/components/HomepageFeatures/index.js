import clsx from 'clsx';
import Heading from '@theme/Heading';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Free',
    description: (
      <>
        YADB is free (as in free beer and freedom), forever. That means
        that you can use it for as long as you like, and adapt it to your needs.
      </>
    ),
  },
  {
    title: 'Focus on What Matters',
    description: (
      <>
        YADB is simple : it integrates into Discord perfectly
        and uses the slash command system. No more looking thru help 
        pages for hours!
      </>
    ),
  },
  {
    title: 'Community centric',
    description: (
      <>
        YADB is made by the community, for the community. No big company
        behind the project.
      </>
    ),
  },
];

function Feature({title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center padding-horiz--md">
        <Heading as="h3">{title}</Heading>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
